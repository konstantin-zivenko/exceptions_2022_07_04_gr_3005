import sys

class MyException(Exception):
    pass

def linux_interaction():
    assert ("win32" in sys.platform), "Function can only run on Linux systems."
    print("Doing something")

try:
    linux_interaction()
    x = float(input("input something:  "))
    if x < 0:
        raise MyException("x is less than 0", 2)
    a = 10 / x

except AssertionError as error:
    print(error)
    print("in exception block 1")
except ZeroDivisionError as error:
    print(error)
    print("in exception block 2")
except MyException as error:
    print(error)
    print("in exception block 3")
# except:
#     print("in exception block 4")
else:
    print("I`m in module else")
finally:
    print("I will work always")